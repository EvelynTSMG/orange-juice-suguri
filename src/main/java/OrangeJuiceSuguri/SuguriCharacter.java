package OrangeJuiceSuguri;

import OrangeJuiceSuguri.cards.Accelerator;
import OrangeJuiceSuguri.cards.BaseCard;
import OrangeJuiceSuguri.cards.Dash;
import OrangeJuiceSuguri.cards.Defend;
import OrangeJuiceSuguri.cards.OnFire;
import OrangeJuiceSuguri.cards.Strike;
import OrangeJuiceSuguri.relics.Dice;
import OrangeJuiceSuguri.util.ChatterHelper;
import basemod.abstracts.CustomEnergyOrb;
import basemod.abstracts.CustomPlayer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.brashmonkey.spriter.Player;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.EnergyManager;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ScreenShake;
import com.megacrit.cardcrawl.localization.CharacterStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.screens.CharSelectInfo;

import java.util.ArrayList;

import static OrangeJuiceSuguri.SuguriCharacter.Enums.ORANGE_JUICE_SUGURI_COLOR;
import static OrangeJuiceSuguri.util.ChatterHelper.ChatterType;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.CORPSE;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.SHOULDER1;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.SHOULDER2;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.charPath;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.char_color;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.alive_monsters;
import static OrangeJuiceSuguri.util.Wiz.room;

public class SuguriCharacter extends CustomPlayer {

    static final String ID = id("Suguri");
    static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString(ID);
    static final String[] NAMES = characterStrings.NAMES;
    static final String[] TEXT = characterStrings.TEXT;
    public static final int HP = 80;
    public static final int ORB_SLOTS = 0;
    public static final int GOLD = 99;
    public static final int CARD_DRAW = 5;


    public SuguriCharacter(String name, PlayerClass setClass) {
        super(
            name,
            setClass,
            new CustomEnergyOrb(orbTextures, charPath("mainChar/orb/vfx.png"), null),
            new CustomSpriterAnimation(charPath("mainChar/basic.scml"))
        );
        Player.PlayerListener listener = new CustomAnimationListener(this);
        ((CustomSpriterAnimation)this.animation).myPlayer.addListener(listener);
        initializeClass(null,
                SHOULDER1,
                SHOULDER2,
                CORPSE,
                getLoadout(), 20.0F, -10.0F, 166.0F, 327.0F, new EnergyManager(3));


        dialogX = (drawX + 0.0F * Settings.scale);
        dialogY = (drawY + 240.0F * Settings.scale);
    }

    @Override
    public CharSelectInfo getLoadout() {
        return new CharSelectInfo(NAMES[0], TEXT[0],
                HP, HP, ORB_SLOTS, GOLD, CARD_DRAW, this, getStartingRelics(),
                getStartingDeck(), false);
    }

    @Override
    public ArrayList<String> getStartingDeck() {
        ArrayList<String> deck = new ArrayList<>();

        for (int i = 0; i < 4; i++) deck.add(Strike.ID);
        deck.add(OnFire.ID);
        for (int i = 0; i < 4; i++) deck.add(Defend.ID);
        deck.add(Dash.ID);

        return deck;
    }

    public ArrayList<String> getStartingRelics() {
        ArrayList<String> relics = new ArrayList<>();
        relics.add(Dice.ID);
        return relics;
    }

    @Override
    public void doCharSelectScreenSelectEffect() {
        CardCrawlGame.sound.playA("UNLOCK_PING", MathUtils.random(-0.2F, 0.2F));
        CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT,
                false);
    }

    private static final String[] orbTextures = {
            charPath("mainChar/orb/layer1.png"),
            charPath("mainChar/orb/layer2.png"),
            charPath("mainChar/orb/layer3.png"),
            charPath("mainChar/orb/layer4.png"),
            charPath("mainChar/orb/layer4.png"),
            charPath("mainChar/orb/layer6.png"),
            charPath("mainChar/orb/layer1d.png"),
            charPath("mainChar/orb/layer2d.png"),
            charPath("mainChar/orb/layer3d.png"),
            charPath("mainChar/orb/layer4d.png"),
            charPath("mainChar/orb/layer5d.png"),
    };

    @Override
    public String getCustomModeCharacterButtonSoundKey() {
        return "UNLOCK_PING";
    }

    @Override
    public int getAscensionMaxHPLoss() {
        return HP/10;
    }

    @Override
    public AbstractCard.CardColor getCardColor() {
        return ORANGE_JUICE_SUGURI_COLOR;
    }

    @Override
    public Color getCardTrailColor() {
        return char_color.cpy();
    }

    @Override
    public BitmapFont getEnergyNumFont() {
        return FontHelper.energyNumFontGreen;
    }

    @Override
    public String getLocalizedCharacterName() {
        return NAMES[0];
    }

    @Override
    public AbstractCard getStartCardForEvent() {
        System.out.println("YOU NEED TO SET getStartCardForEvent() in your " + getClass().getSimpleName() + " file!");
        return null;
    }

    @Override
    public String getTitle(AbstractPlayer.PlayerClass playerClass) {
        return NAMES[1];
    }

    @Override
    public AbstractPlayer newInstance() {
        return new SuguriCharacter(name, chosenClass);
    }

    @Override
    public Color getCardRenderColor() {
        return char_color.cpy();
    }

    @Override
    public Color getSlashAttackColor() {
        return char_color.cpy();
    }

    @Override
    public AbstractGameAction.AttackEffect[] getSpireHeartSlashEffect() {
        return new AbstractGameAction.AttackEffect[]{
                AbstractGameAction.AttackEffect.FIRE,
                AbstractGameAction.AttackEffect.BLUNT_HEAVY,
                AbstractGameAction.AttackEffect.FIRE};
    }

    @Override
    public String getSpireHeartText() {
        return TEXT[1];
    }

    @Override
    public String getVampireText() {
        return TEXT[2];
    }

    @Override
    public void onVictory() {
        super.onVictory();
        playAnimation("happy");
    }

    @Override
    public void useCard(AbstractCard card, AbstractMonster monster, int energyOnUse) {
        super.useCard(card, monster, energyOnUse);
        switch (card.type) {
            case ATTACK:
                ChatterHelper.showChatter(ChatterType.ATTACK);
                if (card instanceof BaseCard && ((BaseCard) card).uses_skill_anim) {
                    playAnimation("skill");
                } else {
                    playAnimation("attack");
                }
                break;

            case SKILL:
                ChatterHelper.showChatter(ChatterType.SKILL);
                playAnimation("skill");
                break;

            case POWER:
                ChatterHelper.showChatter(ChatterType.POWER);
                playAnimation("happy");
                break;
        }
    }

    public void damage(DamageInfo info) {
        super.damage(info);

        if (lastDamageTaken == 0) {
            ChatterHelper.showChatter(ChatterType.BLOCKED_DAMAGE);
            playAnimation("happy");
        } else if (info.output > 0) {
            if (info.owner != null && info.type != DamageInfo.DamageType.THORNS) {
                ChatterHelper.showChatter(info.output > maxHealth/2 ? ChatterType.HIGH_DAMAGE : ChatterType.LOW_DAMAGE);
            } else {
                ChatterHelper.showChatter(ChatterType.FIELD_DAMAGE);
            }
            playAnimation("hurt");
        }
    }

    public CustomSpriterAnimation getAnimation() {
        return (CustomSpriterAnimation) this.animation;
    }

    public void playAnimation(String name) {
        ((CustomSpriterAnimation)this.animation).myPlayer.setAnimation(name);
    }

    public void stopAnimation() {
        CustomSpriterAnimation anim = (CustomSpriterAnimation) this.animation;
        int time = anim.myPlayer.getAnimation().length;
        anim.myPlayer.setTime(time);
        anim.myPlayer.speed = 0;
    }

    public void resetToIdleAnimation() {
        playAnimation("idle");
    }

    @Override
    public void playDeathAnimation() {
        playAnimation("ko");
    }

    @Override
    public void heal(int healAmount) {
        if (healAmount > 0) {
            if (ChatterHelper.showChatter(ChatterType.HEALING)) {
                playAnimation("happy");
            }
        }
        super.heal(healAmount);
    }

    @Override
    public void preBattlePrep() {
        playAnimation("idle");
        super.preBattlePrep();

        boolean bossFight = alive_monsters()
            .stream()
            .anyMatch(mon -> mon.type == AbstractMonster.EnemyType.BOSS);

        if (room().eliteTrigger || bossFight) {
            ChatterHelper.showChatter(ChatterType.BOSS_FIGHT_START);
        } else {
            ChatterHelper.showChatter(isBloodied ? ChatterType.LOW_HP_BATTLE_START : ChatterType.BATTLE_START);
        }
    }

    public static class Enums {
        @SpireEnum
        public static AbstractPlayer.PlayerClass ORANGE_JUICE_SUGURI_CLASS;
        @SpireEnum(name = "ORANGE_JUICE_SUGURI_COLOR")
        public static AbstractCard.CardColor ORANGE_JUICE_SUGURI_COLOR;
        @SpireEnum(name = "ORANGE_JUICE_SUGURI_COLOR")
        @SuppressWarnings("unused")
        public static CardLibrary.LibraryType LIBRARY_COLOR;
    }
}
