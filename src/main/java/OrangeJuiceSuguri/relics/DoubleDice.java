package OrangeJuiceSuguri.relics;

import OrangeJuiceSuguri.OrangeJuiceSuguriMod;
import OrangeJuiceSuguri.powers.EvasionPower;
import com.megacrit.cardcrawl.relics.AbstractRelic;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.*;

public class DoubleDice extends BaseRelic {
    public final static String NAME = DoubleDice.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.BOSS;
    public final static LandingSound SOUND = LandingSound.CLINK;
    public final static int EVASION = 6;

    public DoubleDice() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public void obtain() {
        if (player().hasRelic(Dice.ID)) {
            Dice dice = (Dice)player().getRelic(Dice.ID);
            for (int i = 0; i < player().relics.size(); i++) {
                if (!player().relics.get(i).relicId.equals(Dice.ID)) continue;
                instantObtain(player(), i, true);
                return;
            }
            return;
        }

        super.obtain();
    }

    @Override
    public boolean canSpawn() {
        return player().hasRelic(Dice.ID);
    }

    @Override
    public String getUpdatedDescription() {
        return String.format(DESCRIPTIONS[0], OrangeJuiceSuguriMod.char_color.toString(), evasion_percent(EVASION) * 100);
    }

    @Override
    public void atPreBattle() {
        flash_above_player();
        apply_self_now(new EvasionPower(player(), EVASION));
    }
}