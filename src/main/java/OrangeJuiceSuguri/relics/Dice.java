package OrangeJuiceSuguri.relics;

import OrangeJuiceSuguri.powers.EvasionPower;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.apply_self_now;
import static OrangeJuiceSuguri.util.Wiz.evasion_percent;
import static OrangeJuiceSuguri.util.Wiz.player;

public class Dice extends BaseRelic {
    public final static String NAME = Dice.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.STARTER;
    public final static LandingSound SOUND = LandingSound.CLINK;
    public final static int EVASION = 3;

    public Dice() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public String getUpdatedDescription() {
        return String.format(DESCRIPTIONS[0], evasion_percent(EVASION) * 100);
    }

    @Override
    public void atPreBattle() {
        flash_above_player();
        apply_self_now(new EvasionPower(player(), EVASION));
    }
}