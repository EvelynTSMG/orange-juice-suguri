package OrangeJuiceSuguri.relics;

import OrangeJuiceSuguri.SuguriCharacter;
import OrangeJuiceSuguri.util.TexLoader;
import basemod.abstracts.CustomRelic;
import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.relicPath;
import static OrangeJuiceSuguri.util.Wiz.atb;
import static OrangeJuiceSuguri.util.Wiz.player;

public abstract class BaseRelic extends CustomRelic {
    public CardColor color;

    public BaseRelic(String name, RelicTier tier, LandingSound sfx) {
        this(name, tier, sfx, SuguriCharacter.Enums.ORANGE_JUICE_SUGURI_COLOR);
    }

    public BaseRelic(String name, RelicTier tier, LandingSound sfx, CardColor color) {
        super(id(name), TexLoader.get_texture(relicPath(name + ".png")), tier, sfx);
        outlineImg = TexLoader.get_texture(relicPath("outlines/" + name + ".png"));
        this.color = color;
    }

    public void flash_above_player() {
        atb(new RelicAboveCreatureAction(player(), this));
    }

    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }
}
