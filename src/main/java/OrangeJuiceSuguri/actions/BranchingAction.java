package OrangeJuiceSuguri.actions;

import OrangeJuiceSuguri.util.Option;
import OrangeJuiceSuguri.util.Wiz;
import com.megacrit.cardcrawl.actions.AbstractGameAction;

import java.util.function.Supplier;

import static OrangeJuiceSuguri.util.Option.None;
import static OrangeJuiceSuguri.util.Wiz.att;

/**
 * Action that will run a {@code test},
 * then run {@code on_success} if it succeeded
 * or {@code on_failure} if it failed
 */
public class BranchingAction extends AbstractGameAction {
    public final Supplier<Boolean> test;
    private final AbstractGameAction on_success;
    private final Option<AbstractGameAction> on_failure;

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success) {
        this(test, on_success, None());
    }

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success, AbstractGameAction on_failure) {
        this(test, on_success, Option.fromNullable(on_failure));
    }

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success, Option<AbstractGameAction> on_failure) {
        this.test = test;
        this.on_success = on_success;
        this.on_failure = on_failure;
    }

    public void update() {
        if (test.get()) att(on_success);
        else on_failure.inspect(Wiz::att);

        isDone = true;
    }
}
