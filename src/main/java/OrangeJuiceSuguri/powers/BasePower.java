package OrangeJuiceSuguri.powers;

import OrangeJuiceSuguri.util.Option;
import OrangeJuiceSuguri.util.TexLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.powerPath;
import static OrangeJuiceSuguri.util.Option.None;
import static OrangeJuiceSuguri.util.Option.Some;
import static OrangeJuiceSuguri.util.Wiz.atb;
import static OrangeJuiceSuguri.util.Wiz.att;

public abstract class BasePower extends AbstractPower {
    public Option<Integer> amount2 = None();
    public static Color red = Color.RED.cpy(); // ???
    public static Color green = Color.GREEN.cpy(); // ???
    public final boolean can_be_negative = false;
    public final boolean is_amount_percent;
    public final boolean is_amount2_percent;

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner, int amount) {
        this(name, power_type, turn_based, owner, amount, false, None(), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent) {
        this(name, power_type, turn_based, owner, amount, is_amount_percent, None(), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, int amount2) {
        this(name, power_type, turn_based, owner, amount, false, Some(amount2), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent,
                     int amount2, boolean is_amount2_percent) {
        this(name, power_type, turn_based, owner, amount, is_amount_percent, Some(amount2), is_amount2_percent);
    }

    private BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                      int amount, boolean is_amount_percent,
                      Option<Integer> amount2, boolean is_amount2_percent) {
        this.ID = id(name);

        PowerStrings POWER_STRINGS = CardCrawlGame.languagePack.getPowerStrings(this.ID);
        this.name = POWER_STRINGS.NAME;
        DESCRIPTIONS = POWER_STRINGS.DESCRIPTIONS;

        this.isTurnBased = turn_based;

        this.owner = owner;
        this.amount = amount;
        this.type = power_type;
        this.amount2 = amount2;

        this.is_amount_percent = is_amount_percent;
        this.is_amount2_percent = is_amount2_percent;

        Texture normal_image = TexLoader.get_texture(powerPath("lowres/" + name + ".png"));
        Texture hd_image = TexLoader.get_texture(powerPath("highres/" + name + ".png"));
        if (hd_image != null) {
            region128 = new TextureAtlas.AtlasRegion(hd_image, 0, 0, hd_image.getWidth(), hd_image.getHeight());
        }

        if (normal_image != null) {
            region48 = new TextureAtlas.AtlasRegion(normal_image, 0, 0, normal_image.getWidth(), normal_image.getHeight());
            if (hd_image == null) this.img = normal_image;
        }

        updateDescription();
    }

    public void remove() {
        atb(new RemoveSpecificPowerAction(owner, owner, this));
    }

    public void remove_now() {
        att(new RemoveSpecificPowerAction(owner, owner, this));
    }

    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0];
    }

    @Override
    public void renderAmount(SpriteBatch sb, float x, float y, Color c) {
        final Color orig_c = c.cpy();
        final String amount_text;
        if (is_amount_percent) {
            amount_text = Double.toString((double)amount / 100d) + "%";
        } else {
            amount_text = Integer.toString(amount);
        }

        if (amount > 0) {
            if (!isTurnBased) {
                green.a = c.a;
                c = green;
            }

            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
        } else if (this.amount < 0 && this.canGoNegative) {
            red.a = c.a;
            c = red;
            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
        }

        if (amount2.is_none()) return;
        int amount2 = this.amount2.unwrap();
        c = orig_c;

        final String amount2_text;
        if (is_amount2_percent) {
            amount2_text = Double.toString((double)amount2 / 100d) + "%";
        } else {
            amount2_text = Integer.toString(amount2);
        }

        if (amount2 > 0) {
            if (!isTurnBased) {
                green.a = c.a;
                c = green;
            }
        } else if (amount2 < 0 && can_be_negative) {
            red.a = c.a;
            c = red;
        }

        FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount2_text, x, y + 15.0F * Settings.scale, fontScale, c);
    }
}
