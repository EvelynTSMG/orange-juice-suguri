package OrangeJuiceSuguri.powers;

import OrangeJuiceSuguri.patches.PreDecrementBlockPatches.PreDecrementBlockPower;

import OrangeJuiceSuguri.util.TexLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.powerPath;
import static OrangeJuiceSuguri.util.Wiz.*;

public class EvasionPower extends BasePower implements PreDecrementBlockPower {
    public final static String NAME = EvasionPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    private static final Texture pos_img = TexLoader.get_texture(powerPath("lowres/EvasionPowerPositive.png"));
    private static final Texture neg_img = TexLoader.get_texture(powerPath("lowres/EvasionPowerNegative.png"));

    // Default constructor for AutoAdd
    public EvasionPower() {
        this(AbstractDungeon.player);
    }

    public EvasionPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public EvasionPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount, true);
        canGoNegative = true;
        img = amount < 0 ? neg_img : pos_img;
        region48 = new TextureAtlas.AtlasRegion(img, 0, 0, img.getWidth(), img.getHeight());
    }

    // Funni Professor support
    @Override
    public float atDamageGive(float damage, DamageInfo.DamageType type) {
        return super.atDamageGive(damage, type);
    }

    @Override
    public void updateDescription() {
        // Stupid hackfix
        if (DESCRIPTIONS.length != 5) DESCRIPTIONS = CardCrawlGame.languagePack.getPowerStrings(ID).DESCRIPTIONS;

        float evasion = evasion_percent(amount);
        float minimum = 1 / (float)Math.pow(2, (int)evasion);

        if ((int)evasion == evasion) {
            description = String.format(DESCRIPTIONS[0], minimum * 100);
            return;
        }

        float potential = 1 / (float)Math.pow(2, (int)evasion + Math.signum(evasion));

        if (evasion < 1 && evasion > -1) {
            if (evasion > 0) {
                description = String.format(DESCRIPTIONS[1], Math.abs(evasion * 100), potential * 100);
            } else {
                description = String.format(DESCRIPTIONS[2], Math.abs(evasion * 100), (int)potential);
            }
        } else {
            if (evasion > 0) {
                description = String.format(DESCRIPTIONS[3], minimum * 100, Math.abs((int)evasion - evasion) * 100, potential * 100);
            } else {
                description = String.format(DESCRIPTIONS[4], (int)minimum, Math.abs((int)evasion - evasion) * 100, (int)potential);
            }
        }
    }

    @Override
    public void renderAmount(SpriteBatch sb, float x, float y, Color c) {
        final String amount_text = String.format("%.0f%%", evasion_percent(amount) * 100);

        if (amount > 0) {
            green.a = c.a;
            c = green;
        } else if (amount < 0 && canGoNegative) {
            red.a = c.a;
            c = red;
        }

        FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
    }

    @Override
    public void renderIcons(SpriteBatch sb, float x, float y, Color c) {
        img = amount < 0 ? neg_img : pos_img;
        super.renderIcons(sb, x, y, c);
    }

    @Override
    public int pre_block_calc(AbstractMonster m, DamageInfo info, int damage) {
        float evasion = evasion_percent(amount);
        int hyperevasion = (int)evasion;
        int evasion_coefficient = hyperevasion;
        if (AbstractDungeon.miscRng.randomBoolean(evasion - hyperevasion)) {
            evasion_coefficient += Math.signum(evasion);
        }

        return (int)(damage / (float)Math.pow(2, evasion_coefficient));
    }
}