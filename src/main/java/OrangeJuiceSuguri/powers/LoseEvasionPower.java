package OrangeJuiceSuguri.powers;

import OrangeJuiceSuguri.util.TexLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.ReducePowerAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.powerPath;
import static OrangeJuiceSuguri.util.Wiz.atb;
import static OrangeJuiceSuguri.util.Wiz.evasion_percent;
import static OrangeJuiceSuguri.util.Wiz.player;

public class LoseEvasionPower extends BasePower {
    public static String NAME = LoseEvasionPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.DEBUFF;
    public final static boolean TURN_BASED = true;
    public static final int DEFAULT_AMOUNT = 1;

    private static final Texture pos_img = TexLoader.get_texture(powerPath("lowres/LoseEvasionPowerPositive.png"));
    private static final Texture neg_img = TexLoader.get_texture(powerPath("lowres/LoseEvasionPowerNegative.png"));

    // Default constructor for AutoAdd
    public LoseEvasionPower() {
        this(player());
    }

    public LoseEvasionPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public LoseEvasionPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        canGoNegative = true;
        img = amount < 0 ? neg_img : pos_img;
        region48 = new TextureAtlas.AtlasRegion(img, 0, 0, img.getWidth(), img.getHeight());
    }

    @Override
    public void updateDescription() {
        description = String.format(DESCRIPTIONS[0], evasion_percent(amount) * 100);
    }

    @Override
    public void renderAmount(SpriteBatch sb, float x, float y, Color c) {
        int amount = -this.amount;
        final String amount_text = String.format("%.0f%%", evasion_percent(amount) * 100);

        if (amount > 0) {
            green.a = c.a;
            c = green;
        } else if (amount < 0 && canGoNegative) {
            red.a = c.a;
            c = red;
        }

        FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
    }

    @Override
    public void renderIcons(SpriteBatch sb, float x, float y, Color c) {
        img = amount < 0 ? neg_img : pos_img;
        super.renderIcons(sb, x, y, c);
    }

    @Override
    public void atEndOfRound() {
        flash();
        atb(new ApplyPowerAction(owner, owner, new EvasionPower(owner, -amount), -amount));
        atb(new RemoveSpecificPowerAction(owner, owner, this));
    }
}
