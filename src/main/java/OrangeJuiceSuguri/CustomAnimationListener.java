package OrangeJuiceSuguri;

import basemod.BaseMod;
import com.brashmonkey.spriter.Animation;
import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Player;

public class CustomAnimationListener implements Player.PlayerListener {

    private final SuguriCharacter character;

    public CustomAnimationListener(SuguriCharacter character) {
        this.character = character;
    }

    @Override
    public void animationFinished(Animation animation) {
        BaseMod.logger.info("Animation finished: " + animation.name);
        if (animation.name.equals("ko")) {
            character.playAnimation("ko");
        } else if (!animation.name.equals("idle")) {
            character.resetToIdleAnimation();
        }
    }

    @Override
    public void animationChanged(Animation animation, Animation animation1) { }

    @Override
    public void preProcess(Player player) { }

    @Override
    public void postProcess(Player player) { }

    @Override
    public void mainlineKeyChanged(Mainline.Key key, Mainline.Key key1) { }
}
