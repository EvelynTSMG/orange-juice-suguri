package OrangeJuiceSuguri.util;

import jdk.internal.vm.annotation.ForceInline;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static OrangeJuiceSuguri.util.Result.*;

@SuppressWarnings("unused")
public final class Option<T> {
    private static final Option<?> none = new Option<>(null);
    private T value;

    private Option(T value) {
        this.value = value;
    }

    @ForceInline
    public static <T> Option<T> Some(T value) {
        return new Option<>(value);
    }

    @ForceInline
    public static <T> Option<T> None() {
        @SuppressWarnings("unchecked")
        Option<T> t = (Option<T>)none;
        return t;
    }

    @ForceInline
    public static <T> Option<T> fromNullable(T value) {
        return value != null ? Some(value) : None();
    }

    @ForceInline
    public static <T> Option<T> fromOptional(Optional<T> opt) {
        return opt.isPresent() ? Some(opt.get()) : None();
    }

    @ForceInline
    public boolean is_some() {
        return value != null;
    }

    @ForceInline
    public boolean is_some_and(Predicate<T> f) {
        return value != null && f.test(value);
    }

    @ForceInline
    public boolean is_none() {
        return value == null;
    }

    @ForceInline
    public T expect(String msg) {
        if (value != null) return value;
        throw new UnexpectedStateError(msg);
    }

    @ForceInline
    public T unwrap() {
        if (value != null) return value;
        throw new NoSuchElementException("called `Option::unwrap()` on a `None` value");
    }

    @ForceInline
    public T unwrap_or(T def) {
        return value != null ? value : def;
    }

    @ForceInline
    public T unwrap_or_else(Supplier<T> f) {
        return value != null ? value : f.get();
    }

    // unwrap_or_default is incredibly difficult, if not impossible
    // so I didn't bother

    @ForceInline
    public T unwrap_unchecked() {
        return value;
    }

    @ForceInline
    public <U> Option<U> map(Function<T, U> op) {
        return value != null ? new Option<>(op.apply(value)) : None();
    }

    @ForceInline
    public <U> U map_or(Function<T, U> op, U def) {
        return value != null ? op.apply(value) : def;
    }

    @ForceInline
    public <U> U map_or_else(Function<T, U> op, Supplier<U> def) {
        return value != null ? op.apply(value) : def.get();
    }

    @ForceInline
    public Option<T> inspect(Consumer<? super T> f) {
        if (value != null) f.accept(value);

        return this;
    }

    @ForceInline
    public <E> Result<T, E> ok_or(E error) {
        return value != null ? Ok(value) : Err(error);
    }

    @ForceInline
    public <E> Result<T, E> ok_or_else(Supplier<E> error) {
        return value != null ? Ok(value) : Err(error.get());
    }

    @ForceInline
    public <U> Option<U> and(Option<U> optb) {
        return value != null ? optb : None();
    }

    @ForceInline
    public <U> Option<U> and_then(Function<? super T, Option<U>> f) {
        return value != null ? f.apply(value) : None();
    }

    @ForceInline
    public Option<T> or(Option<T> optb) {
        return value != null ? this : optb;
    }

    @ForceInline
    public Option<T> or_else(Supplier<Option<T>> f) {
        return value != null ? this : f.get();
    }

    @ForceInline
    public Option<T> xor(Option<T> optb) {
        if (value != null && optb.value == null) return this;
        else if (value == null && optb.value != null) return optb;
        return None();
    }

    @ForceInline
    public Option<T> filter(Predicate<T> filter) {
        return value != null && filter.test(value) ? this : None();
    }

    @ForceInline
    public T insert(T value) {
        this.value = value;
        return this.value;
    }

    @ForceInline
    public T get_or_insert(T value) {
        if (this.value == null) this.value = value;
        return this.value;
    }

    @ForceInline
    public T get_or_insert_with(Supplier<T> f) {
        if (this.value == null) this.value = f.get();
        return this.value;
    }

    @ForceInline
    public Option<T> take() {
        Option<T> other = Some(value);
        value = null;
        return other;
    }

    @ForceInline
    public Option<T> take_if(Predicate<T> predicate) {
        return this.map_or(predicate::test, false) ? this.take() : None();
    }

    @ForceInline
    public Option<T> replace(T value) {
        Option<T> old = fromNullable(this.value);
        this.value = value;
        return old;
    }

    // I'm not using reflection to get around Java's stupid limitations
    // just to implement cloned()
}
