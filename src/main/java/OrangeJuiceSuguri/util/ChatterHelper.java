package OrangeJuiceSuguri.util;

import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.UIStrings;
import com.megacrit.cardcrawl.vfx.SpeechBubble;

import java.util.Arrays;
import java.util.List;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.player;
import static OrangeJuiceSuguri.util.Wiz.random_item_unchecked;

public class ChatterHelper {
    private static final float chatter_chance_on_card_use = 0.25f;
    private static final float chatter_chance_on_damage = 0.5f;
    private static final float chatter_chance_on_battle_start = 0.75f;

    public enum ChatterType {
        ATTACK("ChatterAttack", chatter_chance_on_card_use),
        SKILL("ChatterSkill", chatter_chance_on_card_use),
        POWER("ChatterPower", chatter_chance_on_card_use),
        BATTLE_START("ChatterBattleStart", chatter_chance_on_battle_start),
        LOW_HP_BATTLE_START("ChatterLowHPBattleStart", chatter_chance_on_battle_start),
        BOSS_FIGHT_START("ChatterBossFightStart", chatter_chance_on_battle_start),
        HEALING("ChatterHealing", chatter_chance_on_damage),
        FIELD_DAMAGE("ChatterFieldDamage", chatter_chance_on_damage),
        LOW_DAMAGE("ChatterLightDamage", chatter_chance_on_damage),
        HIGH_DAMAGE("ChatterHeavyDamage", chatter_chance_on_damage),
        BLOCKED_DAMAGE("ChatterBlockedDamage", chatter_chance_on_damage);

        public final List<String> chatter;
        public final float probability;

        ChatterType(String chatter_id, float probability) {
            UIStrings chatter_strings = CardCrawlGame.languagePack.getUIString(id(chatter_id));
            chatter = Arrays.asList(chatter_strings.TEXT);
            this.probability = probability;
        }
    }

    /**
     * @param chatter The type of chatter to say
     * @return Returns true if we talked or false if we did not. Can be used to have additional conditionals on chat
     */
    public static boolean showChatter(ChatterType chatter) {
        if (AbstractDungeon.effectList.stream().noneMatch(fx -> fx instanceof SpeechBubble)
                && MathUtils.random(1f) < chatter.probability) {
            AbstractDungeon.effectList.add(
                new SpeechBubble(
                    player().dialogX,
                    player().dialogY,
                    2f,
                    random_item_unchecked(chatter.chatter),
                    true));
            return true;
        }

        return false;
    }

    public static void showPlayerChatter(String chatter) {
        AbstractDungeon.effectList.add(new SpeechBubble(player().dialogX, player().dialogY, 2f, chatter, true));
    }
}
