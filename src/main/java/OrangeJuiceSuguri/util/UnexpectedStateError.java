package OrangeJuiceSuguri.util;

public class UnexpectedStateError extends Error {
    public UnexpectedStateError(String msg) {
        super(msg);
    }
}
