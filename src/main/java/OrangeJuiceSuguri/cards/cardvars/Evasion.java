package OrangeJuiceSuguri.cards.cardvars;

import OrangeJuiceSuguri.cards.BaseCard;
import basemod.abstracts.DynamicVariable;
import com.megacrit.cardcrawl.cards.AbstractCard;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.evasion_percent;

public class Evasion extends DynamicVariable {
    @Override
    public String key() {
        return id("EVA");
    }

    @Override
    public boolean isModified(AbstractCard card) {
        if (card instanceof BaseCard) {
            return ((BaseCard) card).is_evasion_modified;
        }
        return false;
    }

    @Override
    public void setIsModified(AbstractCard card, boolean v) {
        if (card instanceof BaseCard) {
            ((BaseCard) card).is_evasion_modified = v;
        }
    }

    @Override
    public int value(AbstractCard card) {
        if (card instanceof BaseCard) {
            return (int)(evasion_percent(((BaseCard) card).evasion) * 100);
        }
        return -1;
    }

    @Override
    public int baseValue(AbstractCard card) {
        if (card instanceof BaseCard) {
            return (int)(evasion_percent(((BaseCard) card).base_evasion) * 100);
        }
        return -1;
    }

    @Override
    public boolean upgraded(AbstractCard card) {
        if (card instanceof BaseCard) {
            return ((BaseCard) card).upp_evasion;
        }
        return false;
    }
}
