package OrangeJuiceSuguri.cards;

import OrangeJuiceSuguri.cards.BaseCard;

import OrangeJuiceSuguri.powers.EvasionPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;
import com.megacrit.cardcrawl.powers.LoseDexterityPower;
import com.megacrit.cardcrawl.powers.LoseStrengthPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.*;

public class OnFire extends BaseCard {
    public final static String NAME = OnFire.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 2;
    public final static int DMG = 13;
    public final static int UP_DMG = 4;
    public final static int EVASION = 2;

    public OnFire() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        evasion(EVASION);
        updateDescription();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.FIRE);
        apply_enemy(m, new EvasionPower(m, -evasion));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}