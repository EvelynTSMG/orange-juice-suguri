package OrangeJuiceSuguri.cards;

import OrangeJuiceSuguri.actions.InstantAction;
import OrangeJuiceSuguri.cards.BaseCard;

import basemod.cardmods.EtherealMod;
import basemod.cardmods.ExhaustMod;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.*;

public class Accelerator extends BaseCard {
    public final static String NAME = Accelerator.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Accelerator() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        // ugly but q:
        atb(new DrawCardAction(magicNumber, new InstantAction(() -> {
            for (AbstractCard c : DrawCardAction.drawnCards) {
                c.setCostForTurn(c.costForTurn - 1);
                CardModifierManager.addModifier(c, new EtherealMod() {
                    @Override
                    public boolean removeAtEndOfTurn(AbstractCard card) {
                        return true;
                    }
                });
            }
        })));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}