package OrangeJuiceSuguri.cards;

import OrangeJuiceSuguri.cards.BaseCard;

import OrangeJuiceSuguri.powers.EvasionPower;
import OrangeJuiceSuguri.powers.LoseEvasionPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static OrangeJuiceSuguri.OrangeJuiceSuguriMod.id;
import static OrangeJuiceSuguri.util.Wiz.*;

public class Dash extends BaseCard {
    public final static String NAME = Dash.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 0;
    public final static int EVASION = 3;
    public final static int UP_EVASION = 2;

    public Dash() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        evasion(EVASION);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new EvasionPower(p, evasion));
        apply_self(new LoseEvasionPower(p, evasion));
    }

    @Override
    public void upp() {
        upgradeEvasion(UP_EVASION);
        updateDescription();
    }
}