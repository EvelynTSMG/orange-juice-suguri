package OrangeJuiceSuguri;

import OrangeJuiceSuguri.cards.BaseCard;
import OrangeJuiceSuguri.cards.cardvars.Evasion;
import OrangeJuiceSuguri.cards.cardvars.SecondDamage;
import OrangeJuiceSuguri.cards.cardvars.SecondMagicNumber;
import OrangeJuiceSuguri.relics.BaseRelic;
import basemod.AutoAdd;
import basemod.BaseMod;
import basemod.abstracts.DynamicVariable;
import basemod.helpers.RelicType;
import basemod.interfaces.EditCardsSubscriber;
import basemod.interfaces.EditCharactersSubscriber;
import basemod.interfaces.EditKeywordsSubscriber;
import basemod.interfaces.EditRelicsSubscriber;
import basemod.interfaces.EditStringsSubscriber;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.evacipated.cardcrawl.mod.stslib.Keyword;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.google.gson.Gson;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.localization.CharacterStrings;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.localization.RelicStrings;
import com.megacrit.cardcrawl.localization.UIStrings;
import com.megacrit.cardcrawl.unlock.UnlockTracker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;

import static java.awt.SystemColor.info;

@SuppressWarnings({"unused", "WeakerAccess"})
@SpireInitializer
public class OrangeJuiceSuguriMod implements
        EditCardsSubscriber,
        EditRelicsSubscriber,
        EditStringsSubscriber,
        EditKeywordsSubscriber,
        EditCharactersSubscriber {

    public static final String MOD_ID = "OrangeJuiceSuguri";
    public static final Logger logger = LogManager.getLogger(MOD_ID);

    // 5% Evasion halved = 0, not 2.5%
    public static final float EVASION_PER_INT = 0.05f;

    public static String id(String idText) {
        return MOD_ID + ":" + idText;
    }

    public static String path(String resourcePath) {
        return MOD_ID + "Resources/" + resourcePath;
    }

    public static String imagePath(String resourcePath) {
        return path("images/" + resourcePath);
    }

    public static String relicPath(String resourcePath) {
        return imagePath("relics/" + resourcePath);
    }

    public static String powerPath(String resourcePath) {
        return imagePath("powers/" + resourcePath);
    }

    public static String charPath(String resourcePath) {
        return imagePath("char/" + resourcePath);
    }

    public static String cardPath(String resourcePath) {
        return imagePath("cards/" + resourcePath);
    }

    public static String langPath(String resourcePath) {
        return path("localization/" + getLangString() + "/" + resourcePath);
    }

    public static Color char_color = Color.valueOf("efefef");

    public static final String SHOULDER1 = charPath("mainChar/shoulder.png");
    public static final String SHOULDER2 = charPath("mainChar/shoulder2.png");
    public static final String CORPSE = charPath("mainChar/battle/ko.png");
    private static final String ATTACK_S_ART = imagePath("512/attack.png");
    private static final String SKILL_S_ART = imagePath("512/skill.png");
    private static final String POWER_S_ART = imagePath("512/power.png");
    private static final String CARD_ENERGY_S = imagePath("512/energy.png");
    private static final String TEXT_ENERGY = imagePath("512/text_energy.png");
    private static final String ATTACK_L_ART = imagePath("1024/attack.png");
    private static final String SKILL_L_ART = imagePath("1024/skill.png");
    private static final String POWER_L_ART = imagePath("1024/power.png");
    private static final String CARD_ENERGY_L = imagePath("1024/energy.png");
    private static final String CHARSELECT_BUTTON = imagePath("charSelect/charButton.png");
    private static final String CHARSELECT_PORTRAIT = imagePath("charSelect/charBG.png");

    // If you support any more languages, add them here
    public static Settings.GameLanguage[] SupportedLanguages = {
            Settings.GameLanguage.ENG,
    };

    private static String getLangString() {
        for (Settings.GameLanguage lang : SupportedLanguages) {
            if (lang.equals(Settings.language)) {
                return Settings.language.name().toLowerCase();
            }
        }
        return "eng";
    }

    public OrangeJuiceSuguriMod() {
        BaseMod.subscribe(this);

        BaseMod.addColor(SuguriCharacter.Enums.ORANGE_JUICE_SUGURI_COLOR, char_color, char_color, char_color,
            char_color, char_color, char_color, char_color,
                ATTACK_S_ART, SKILL_S_ART, POWER_S_ART, CARD_ENERGY_S,
                ATTACK_L_ART, SKILL_L_ART, POWER_L_ART,
                CARD_ENERGY_L, TEXT_ENERGY);
    }

    public static void initialize() {
        OrangeJuiceSuguriMod mod = new OrangeJuiceSuguriMod();
    }

    @Override
    public void receiveEditCharacters() {
        BaseMod.addCharacter(new SuguriCharacter(SuguriCharacter.characterStrings.NAMES[1], SuguriCharacter.Enums.ORANGE_JUICE_SUGURI_CLASS),
                CHARSELECT_BUTTON, CHARSELECT_PORTRAIT, SuguriCharacter.Enums.ORANGE_JUICE_SUGURI_CLASS);
    }

    @Override
    public void receiveEditRelics() {
        new AutoAdd(MOD_ID)
                .packageFilter(BaseRelic.class)
                .any(BaseRelic.class, (info, relic) -> {
                    if (relic.color == null) BaseMod.addRelic(relic, RelicType.SHARED);
                    else BaseMod.addRelicToCustomPool(relic, relic.color);
                    if (!info.seen) UnlockTracker.markRelicAsSeen(relic.relicId);
                });
    }

    @Override
    public void receiveEditCards() {
        BaseMod.addDynamicVariable(new SecondDamage());
        BaseMod.addDynamicVariable(new SecondMagicNumber());
        BaseMod.addDynamicVariable(new Evasion());
        new AutoAdd(MOD_ID)
                .packageFilter(BaseCard.class)
                .setDefaultSeen(true)
                .cards();
    }

    @Override
    public void receiveEditStrings() {
        BaseMod.loadCustomStringsFile(CardStrings.class, langPath("cards.json"));
        BaseMod.loadCustomStringsFile(RelicStrings.class, langPath("relics.json"));
        BaseMod.loadCustomStringsFile(CharacterStrings.class, langPath("char.json"));
        BaseMod.loadCustomStringsFile(PowerStrings.class, langPath("powers.json"));
        BaseMod.loadCustomStringsFile(UIStrings.class, langPath("chatter.json"));
    }

    @Override
    public void receiveEditKeywords() {
        Gson gson = new Gson();
        String json = Gdx.files.internal(langPath("keywords.json"))
                .readString(String.valueOf(StandardCharsets.UTF_8));
        Keyword[] keywords = gson.fromJson(json, Keyword[].class);

        if (keywords != null) {
            for (Keyword keyword : keywords) {
                BaseMod.addKeyword(MOD_ID.toLowerCase(), keyword.PROPER_NAME, keyword.NAMES, keyword.DESCRIPTION);
            }
        }
    }
}
